const mongoose = require( 'mongoose' );

const autoIncrement = require('mongoose-auto-increment');

var connection = mongoose.createConnection( 'mongodb+srv://administrator:YjGkCxDKcXj9vJx9TVa6dyCBFxAIJrbD@cluster0-fzzji.mongodb.net/goodgms?retryWrites=true', {
        keepAlive: 360000,
        connectTimeoutMS: 360000,
        socketTimeoutMS: 360000, 
        useNewUrlParser: true
    } );
    // administrator
    // YjGkCxDKcXj9vJx9TVa6dyCBFxAIJrbD

mongoose.connection =  connection;

connection.on(  'error', err => console.warn( 'Ошибка подключения к базе данных!\n', err ) );
connection.on( 'connected', res => console.log( 'DB loaded!' ) );

autoIncrement.initialize( connection );

module.exports = connection;