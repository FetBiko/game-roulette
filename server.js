process.on( 'unhandledRejection', console.error );

global.CONFIG  = require( './config' );
global.__rootname = __dirname;

// require( 'module-alias/register' );

// Запускаем сервер.
require( './database' );
require( './app' );
