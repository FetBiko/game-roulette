// Грузим всякие чтуки, которые помогут работать серверу хорошо.
const Koa           = require( 'koa' );
const Pug           = require( 'koa-pug' );

const serve         = require( 'koa-static' );
const logger        = require( 'koa-logger' );
const bodyparser    = require( 'koa-bodyparser' )

const router = require( './src/router' );
const errorHandler = require( './src/middleware/error-handler' );

// Создаём сервер и настраиваем.
var app = new Koa();

// Настраиваем рендерер.
new Pug({
    viewPath: __dirname + '/src/server/views',
    debug: false,
    pretty: true,
    app,
    basedir: __dirname + '/src/server/views',
    locals: { 
        page: 'GOOD GAMES | Крутани рулетку — испытай удачу!',
    }
})

//Error handling middleware

// Применяем плагины и запускаем сервер.
app
    .use( logger() )
    .use( errorHandler() )
    .use( bodyparser() )
    .use( serve( __dirname + '/public', { gzip: true, } ) )
    // .use( serve( __dirname + '/tmp', { gzip: true, } ) )

    // custom middleware.

    // Apply routes.
    .use( router.routes() )
    .use( router.allowedMethods() )

    // Run server!
    // .listen( process.env.PORT || 3000, ( e ) => {

    //     console.log( 'Server launched!\nOpen: ' + CONFIG.general.home_url );
    // } );

    .listen( 3000 )
    
module.exports = app.callback();