var config = {};

const general = config.general = {

    name: 'WIN GAMES',
    // home_url: 'http://localhost:3000',
    home_url: 'https://wingms.ru',
}

const roulette = config.roulette = {

    rollPrice: 59,
    sellPrice: 29,
    topGameChance: .7
}

const authorization = config.authorization = {

    // private_key: require('fs').readFileSync( './secret/jwtRS256.key' ),
    private_key: 'ixlpmi9pwu3k4vtrdlg83f1f02c5krxl7w7iwk77jejx0flcli9k6rznaczs3um9AAA',
    
    vk: {
        client_id: 6778403,
        client_secret: 'm30gO6SsRCU09kSEhPY7',
        redirect_uri: general.home_url + '/auth/login/vk',
        display: 'popup'
    },

    oauth: {
        user: {
            client_id: 6778403,
            redirect_uri: general.home_url + '/auth/login/vk',
            response_type: 'code',
            scope: [].join( ',' )
        }
    }
}

const payments = config.payments = {
    shopId: 1523,
    currency: 3, // 3 - рубли.
    secret: '65r6f2ipzovie0jxd8pfyko7x7awq6qzqgi9icojone6ek3nnotac7xld4rssv13',
    remote: [ '109.120.152.109', '145.239.84.249' ],
    description: 'Пополнение баланса'
}

// const balance = config.balance = {

//     bonuses: [
//         [ 99, 9 ]
//         [ 499, 99 ]
//         [ 999, 249 ],
//     ]
// }

module.exports = config;