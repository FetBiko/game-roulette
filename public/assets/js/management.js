
$( 'button[id="user_submit"]').click( function() {

    var balance = parseInt( $( 'input[name="balance"]' ).val() );
    var role = $( 'select[name="role"]' ).val();
    var isTwisted = $( 'input[name="isTwisted"]')[0].checked;

    updateUser( PROFILE, { balance: balance, role: role, isTwisted: isTwisted } );
});


$( 'a[id="game_delete"]').click( function( ev ) {

    var btn = ev.target;
    var gameid = btn.getAttribute( 'gameid' );

    if( confirm( 'Действително удалить игру ' + gameid + '?\n\nЭто  уничтожит все ключи, загруженные для этой игры!' ) ) {
        
        deleteGame( gameid );
    }
});


$( 'button[id="game_update"]').click( function() {

    var title = $( 'input[name="game_title"]' ).val();
    var image = $( 'input[name="game_image"]' ).val();
    var liveImage = $( 'input[name="game_live_image"]' ).val();
    var steamPage = $( 'input[name="game_steamPage"]').val();
    var steamPrice = parseFloat( $( 'input[name="game_steamPrice"]').val() );
    var sellPrice = parseFloat( $( 'input[name="game_sellPrice"]').val() );
    var isTwisted = $( 'input[name="game_isTwisted"]')[0].checked;

    updateGame( GAME, { title: title, image: image, liveImage: liveImage, steamPage: steamPage, steamPrice: steamPrice, sellPrice: sellPrice, isTwisted: isTwisted } );
});

$( 'button#add_keys').click( function() {

    var keys = $( 'textarea[name="keys"]' ).val();
    var games = $( '#games_select' ).val()
    addKeys( { keys: keys, games: games } );
});

$( 'button[id="game_create"]').click( function() {

    var title = $( 'input[name="ngame_title"]' ).val();
    var image = $( 'input[name="ngame_image"]' ).val();
    var liveImage = $( 'input[name="ngame_live_image"]' ).val();
    var steamPage = $( 'input[name="ngame_steamPage"]').val();
    var steamPrice = parseFloat( $( 'input[name="ngame_steamPrice"]').val() );
    var sellPrice = parseFloat( $( 'input[name="ngame_sellPrice"]').val() );
    var isTwisted = $( 'input[name="ngame_isTwisted"]')[0].checked;

    createGame( { title: title, image: image,  liveImage: liveImage,steamPage: steamPage, steamPrice: steamPrice, sellPrice: sellPrice, isTwisted: isTwisted } );
});

var refreshPage  = function( result ) {

    if( result.success && !result.errors && !result.error )
        document.location = document.location;
}

var formResponseHandler = function( next ) {
    
    return function( result ) {
        console.log( result );

        if( result.error ) {

            $( '#error' )[0].innerText = result.error.message;
        } else if( result.errors ) {

            $( '#error' )[0].innerHTML = Object.keys( result.errors )
                .map( function( err ) { return result.errors[err].message } )
                .join( '</br>' );

        } else $( '#success' )[0].innerText = 'Сохранено!';

        next( result );
    }
}

var updateUser = function( userid, data ) {
    $( '#success' )[0].innerText = ''
    $( '#error' )[0].innerText = '';

    $.ajax({
        url: '/management/users/' + userid,
        type: 'POST',
        data: data,
        success: formResponseHandler( refreshPage )
    });

}

var updateGame = function( gameid, data ) {

    $( '#success' )[0].innerText = ''
    $( '#error' )[0].innerText = '';

    $.ajax({
        url: '/management/games/' + gameid,
        type: 'POST',
        data: data,
        success: formResponseHandler( refreshPage )
    });

}

var createGame = function( data ) {

    $( '#success' )[0].innerText = ''
    $( '#error' )[0].innerText = '';

    $.ajax({
        url: '/management/games/',
        type: 'POST',
        data: data,
        success: formResponseHandler( refreshPage )
    });
}

var deleteGame = function( gameid ) {

    $.ajax({
        url: '/management/games/' + gameid,
        type: 'DELETE',
        success: formResponseHandler( refreshPage )
    });
}

var addKeys = function( data ) {

    console.log( data );

    $( '#success' )[0].innerText = ''
    $( '#error' )[0].innerText = '';

    $.ajax({
        url: '/management/keys/',
        type: 'POST',
        data: data,
        success: formResponseHandler( refreshPage )
    });
}

function qs (params ) { return ( Object.keys( params ).map(key => key + '=' + params[key]).join('&') ); }

function qp ( queryString ) {
    var query = {};
    var pairs = ( queryString[0] === '?' ? queryString.substr(1) : queryString ).split('&');

	if( !pairs  || pairs.length === 0 || pairs[0] === '' )
		return false;

	console.log( pairs );
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }

    return query;
}

$( '#search_button' ).click( function() {

    var searchVal = $( 'input[name="table_search"]' ).val();

    if( !searchVal || searchVal == '{}' )
        searchVal = '';

    var q = qp( document.location.search );

    q.search = searchVal;

    document.location.search = qs( q );
    
} )


var selectTwisted = function() {

    $('#games_select' ).val( Array.from( $( 'option.bg-yellow' ) ).map( function( item ) { return item.value } ) );
}

$( '#select_twisted' ).click( selectTwisted );

var saveMultiplier = function()  {
    
    var multiplier = $( 'input[name="balance_multiplier"]' ).val();
    $( '#error' ).text( '' );
    $( '#success' ).text( '' );

    $.ajax({
        url: '/management/multiplier',
        type: 'POST',
        data: { multiplier: multiplier },
        success: function( response ) {

            if( response.errors ) {

                if( response.errors.indexOf( 'invalid_multiplier' ) !== -1 ) {
                    $( '#error' ).text( 'Неверное значение!' );
                } else $( '#error' ).text( 'Неизвестная ошибка!' );

            } else $( '#success' )[0].innerText = 'Сохранено!'; 
        }
    });
}

$( '#multiplier_save' ).click( saveMultiplier );


var saveRegbonus = function()  {
    
    var regbonus = $( 'input[name="regbonus"]' ).val();
    $( '#regbonus #error' ).text( '' );
    $( '#regbonus #success' ).text( '' );

    $.ajax({
        url: '/management/regbonus',
        type: 'POST',
        data: { regbonus: regbonus },
        success: function( response ) {

            if( response.errors ) {

                if( response.errors.indexOf( 'invalid_regbonus' ) !== -1 ) {
                    $( '#regbonus #error' ).text( 'Неверное значение!' );
                } else $( '#regbonus #error' ).text( 'Неизвестная ошибка!' );

            } else $( '#success' )[0].innerText = 'Сохранено!'; 
        }
    });
}

$( '#regbonus_save' ).click( saveRegbonus );