var Upload = function ( file ) {

    this.file = file;
};

Upload.prototype.upload = function ( opts ) {
    var that = this;
    var formData = new FormData();

    // add assoc key values, this will be posts values
    formData.append( "file", this.file, this.getName() );
    formData.append( "upload_file", true );

    $.ajax({
        type: "POST",
        url: "/management/uploads/image",
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();

            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', that.progressHandling, false);
            }

            return myXhr;
        },
        success: function ( data ) {

            if( typeof opts.success === 'function' )
                opts.success( data ); 
            // your callback here
        },
        error: function ( error ) {
            if( typeof opts.error === 'function' )
                opts.success( error );
        },

        async: true,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000
    });
};

Upload.prototype.progressHandling = function (event) {
    var percent = 0;
    var position = event.loaded || event.position;
    var total = event.total;
    var progress_bar_id = "#progress-wrp";
    if (event.lengthComputable) {
        percent = Math.ceil(position / total * 100);
    }
    // update progressbars classes so it fits your code
    $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
    $(progress_bar_id + " .status").text(percent + "%");
};