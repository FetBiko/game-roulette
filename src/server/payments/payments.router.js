const router        = require( 'koa-router' )();
const controller    = require( './payments.controller' );

// router.use( controller.parseParams );

const authorizaton = require( '../../middleware' ).authorizaton;

router
    // .param( 'community',    controller.paramCommunity )
    .post(  '/success',                 controller.success )
    .get(  '/payment',  authorizaton.authorization(), authorizaton.requireLogin( '/auth/login' ), controller.payment )
    .post(  '/error',                   controller.error )
    .post(  '/interaction',             controller.interaction )

    // .get( '/:id/',              controller.parseParams, controller.delivery )
    // .patch( '/:id',             controller.parseParams, controller.updateDelivery )
    // .delete( '/:id',            controller.parseParams, controller.deleteDelivery )

    // .post( '/:id/cancel',       controller.parseParams, controller.cancelDelivery )
    // .post( '/:id/send',         controller.parseParams, controller.sendDelivery )
    // .post( '/:id/test',         controller.parseParams, controller.testDelivery )


module.exports = router;