const User = require( '../../models/user')
const ServerStatistics = require( '../../models/server-statistics')

var isValidSingature = ( payload ) => {

    console.log( 'PAYLOAD', payload );

    // if( !CONFIG.payments.remote.includes( payload.remote_addr ) )
    //     return false;

    console.log( 'CURRENCY', CONFIG.payments.currency.toString() !== payload.currency.toString() )
    if( CONFIG.payments.currency.toString() !== payload.currency.toString() )
        return false;

    const SIGN = payload.sign;

    var signature = payload;
    delete signature.sign;

    signature = Object.keys( signature )
        .sort()
        .map( key => signature[key] );

    signature.push( CONFIG.payments.secret );
    signature = signature.join( ':' );
    signature = require( 'crypto' ).createHash('sha256').update( signature ).digest( 'hex' );

    console.log( '\nRETURN', signature, SIGN,  '\n' );
    
    return signature === SIGN;
}

class PaymentsController {

    static async payment( $ ) {

        var amount = $.query;

        if( !amount || !amount.amount || isNaN( amount.amount ) || amount.amount < 1 )
            return $.body = { errors: [ 'invalid_amount' ] }

        amount = parseFloat( amount.amount );

        var user = $.isAuthenticated.id;

        var payments = {
            shop: CONFIG.payments.shopId,
            payment: user + '' + Date.now().toString().substr( 5 ),
            currency: CONFIG.payments.currency,
            description: CONFIG.payments.description,
            user,
            amount
        }

        var signature = Object.keys( payments )
            .sort()
            .map( key => payments[key] );

        signature.push( CONFIG.payments.secret );
        signature = signature.join( ':' );
        signature = require( 'crypto' ).createHash('sha256').update( signature ).digest( 'hex' );
        
        payments.sign = signature;

        var stats = await ServerStatistics.getInstance();

        var bonus = ( amount * stats.balanceMultiplier ) - amount;

        console.log( amount, stats.balanceMultiplier );

        return $.render( 'payment-form', {
            payments,
            bonus
        });
    }

    static async success( $ ) {
        var parameters = $.request.body;

        $.redirect( '/' );
    }

    static async error( $ ) {
        var parameters = $.request.body;
        $.redirect( '/' );
    }

    static async interaction( $ ) {

        var parameters = $.request.body;

        if( !parameters || !isValidSingature( parameters ) )
            throw new Error( 'Неверная подпись запроса!' );

        var user = await User.findOne( { id: parameters.uv_user } );

        if( !user )
            throw new Error( 'Пользователь не найден: ' + parameters.uv_user );

        var stats = await ServerStatistics.getInstance();

        console.log( 'balanceMultiplier', stats.balanceMultiplier );

        var balance = user.balance + ( Number( parameters.amount ) * ( stats.balanceMultiplier || 1 ) );
        await user.update( { balance  } );

        console.log( 'balance', balance );

        $.status = 200;
        $.body = { state: parameters.amount }
    }
}

module.exports = PaymentsController;