const router        = require( 'koa-router' )();
const controller    = require( './roulette.controller' );

// router
//     .param( 'community',    controller.paramCommunity )

router
    .post( '/roll',             controller.roll         )
    .post( '/statistics',       controller.statistics   )
    .post( '/sell',             controller.sell         )
    .post( '/key',              controller.key          )

module.exports = router;