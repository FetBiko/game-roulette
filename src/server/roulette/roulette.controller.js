const User = require( '../../models/user' );
const Game = require( '../../models/game' );
const Win = require( '../../models/win' );

const ServerStatistics = require( '../../models/server-statistics' )

var names =  ["Олег Кузнецов","Арсений Меркулов","Василий Смирнов","Максим Максимов","Александр Ананич","Александр Негадов","Вася Калашников","Макс Белдига","Александр Мишуков","Александр Соколиков","Александр Рой","Сергей Крастом","Гламурный Порошочег","Дмитрий Татаринов","Елена Попенко","Лёша Nick","Иван Кокшаров","Юрий Юрьев","Алексей Мельников","Владислав Богуславский","Женя Трусов","Николай Максимов","Олег Нестратов","Антон Котов","Ярослав Даричев","Александр Чужов","Александр Прядеин","Миха Шокот","Артём Масленицын","Кирилл Лысенко","Александр Васильев","Никитос Бойко","Славик Марков","Никита Иванов","Александр Герасимов","Артем Павлов","Алексей Калиничев","Юрий Ванторин","Alex Devian","Ильяс Рулевяйнен","Сергей Гвоздков","Артём Накидонский","Александр Конон","Алексей Ординарцев","Никита Белунин","Andrew Kirillov","Александр Боясов","Максим Решетов","Святослав Олійник","Роман Саратовский","Вова Колпаков","Егор Zimenok","Вячеслав Плехов","Дима Колацк","Серега Юдаков","Ваня Мишуткин","Полина Булыгина","Тимур Печёный","Макс Белый","Александр Вегеле","Дмитрий Иванов","Санька Осадчук","Наташенька Kochelaba","Вика Гортэ","Yaroslav Shportko","Дмитрий Гридасов","Павел Михайлицкий","Женя Васильев","Ваня Васильев","Никита Губанов","Олег Назаров","Андрей Журавлёв","Григорий Квасов","Александр Никитин","Евгений Яремчук","Bulat Nazmutdinov","Дмитрий Груздев","Антон Шаломаев","Оксана Отрощенко","Даниил Лис","Руслан Шайхенуров","Дима Трубин","Илья Щеглов","Амир Давлетшин","Артем Намаконов","Роман Аколупин","Василий Шабанов","Петя Териков","Никитос Никулин","Алексей Самсонов","Дмитрий Родин","Максим Никулин","Юрий Дзюбленко","Иван Байгушев","Андрей Шуюпов","Орест Слота","Валерия Писаревская","Дмитрий Лихачев","Никита Лебедев","Роман Хлобыстов","Андрей Шевченко","Іван Дроник" ];

var demoResponse = async ( warn ) => {
    var games = await Game.find( { isTwisted: true } ).catch( console.error )
    var rolledGame = games[ Math.floor( Math.random() * games.length ) ];
    
    return {
        game: {
            id: rolledGame.id,
            image: rolledGame.image,
            title: rolledGame.title,
            price: rolledGame.steamPrice || false
        },
        isDemo: true,
        warn
    }
}
class RouletteController {

    static async roll( $ ) {

        $.set('Cache-Control', 'no-store, no-cache, must-revalidate');

        var user = $.isAuthenticated;

        if( !user )
            return $.body = await demoResponse( 'need_login' );

        if( user.balance >= CONFIG.roulette.rollPrice ) {

            var topGame = user.isTwisted ? 
                Math.random() < CONFIG.roulette.topGameChance ?
                    true : false
                : false;

            var rolledGame = null, games = null;

            if( topGame ) {
                games = await Game.find( { isTwisted: true, keys: { $exists: true, $not: { $size: 0 } } } ).catch( console.error );
            } else  {

                games = await Game.find( { isTwisted: false, keys: { $exists: true, $not: { $size: 0 } } } ).catch( console.error );
            }

            console.log( games );

            rolledGame = games[ Math.floor( Math.random() * games.length ) ];
            console.log( rolledGame );

            if( !rolledGame )  return $.body = { errors: [ 'internal_error' ] }

            var response = {
                game: {
                    id: rolledGame.id,
                    image: rolledGame.image,
                    title: rolledGame.title,
                    price: rolledGame.steamPrice || false,
                    sellPrice: rolledGame.sellPrice || CONFIG.roulette.sellPrice
                },
                balance: user.balance - CONFIG.roulette.rollPrice
            }

            var win = await Win.create( {
                user: user.id,
                game: rolledGame.id,
                isCompleted: false
            })

            response.win = win.id;

            var user = await User.findOneAndUpdate( { id: user.id }, { balance: response.balance, games: ( user.games || 0 ) + 1, wins: user.wins } )
                .catch( ( error => {

                    console.error( 'BALANCE NOT UPDATED!', error )
                }));
            
            try {
                var statistics = await ServerStatistics.getInstance();

                var createFake = async () => {

                    var statistics = await ServerStatistics.getInstance();
                    var topGame = await Game.find( { isTwisted: true, keys: { $exists: true, $not: { $size: 0 } } } ).catch( console.error );

                    topGame = topGame[ Math.floor( Math.random() * topGame.length )];
                    statistics.liveSum += topGame.price || 499;
                    statistics.topGames += 1;

                    console.log( topGame )
                    statistics.pushGame( ( topGame.liveImage || topGame.image ), names[ Math.floor( Math.random() * names.length )] );

                    await statistics.save();

                }

                // C шансом в 30% добавляется ТОП игра, если пользователь выиграл обычную игру.
                if( Math.random() < CONFIG.roulette.topGameChance && !rolledGame.isTwisted ) {

                    setTimeout( createFake, Math.random() * 1000 * 120 + 1000 * 150 );
                }

                if( Math.random() > .5 ) {
                    setTimeout( createFake, Math.random() * 1000 * 120 + 1000 * 30 );
                }

                // Добавляется игра,  которую  выиграл пользователь.
                
                statistics.liveSum += response.game.price || 249;
                statistics.pushGame( rolledGame.liveImage, user.first_name + ' ' + user.last_name );
                
                if( rolledGame.isTwisted )
                    statistics.topGames += 1;

                console.log( rolledGame.isTwisted );

                statistics = await statistics.save();
            } catch( e ) {  console.log( 'statistics error', e  )}

            return $.body = response;

        } else return $.body = await demoResponse( 'balance_low' );
    
    
    }

    static async key( $ ) {

        $.set('Cache-Control', 'no-store, no-cache, must-revalidate');

        var win = await Win.findOne( { id: ( $.request.body.winid || -1 ) } );

        if( !win || win.user !== $.isAuthenticated.id )
            return { errors: [ 'access_denied' ] }

        if( win.isCompleted )
            return { errors: [ 'win_completed' ] }

        var game = await Game.findOne( { id: ( win.game || -1 ) } );

        if( !game )
            return { errors: [ 'unknown_game' ] }

        
        var keys = game.keys;
        var key = keys.pop();

        await game.update( { keys } );
        await win.update( { isCompleted: true, key } );

        $.body = { key }
    }

    static async sell( $ ) {

        $.set('Cache-Control', 'no-store, no-cache, must-revalidate');

        var win = await Win.findOne( { id: ( $.request.body.winid || -1 ) } );
        
        if( !win || win.user !== $.isAuthenticated.id )
            return { errors: [ 'access_denied' ] }

        if( win.isCompleted )
            return { errors: [ 'win_completed' ] }

        var user = await User.findOne( { id: $.isAuthenticated.id } );
        var game = await Game.findOne( { id: win.game  } );

        await win.update( { isCompleted: true } );

        var updated = user.balance + ( game.sellPrice || CONFIG.roulette.sellPrice );
        await user.update( { balance: updated } );
        
        return $.body = { balance: updated };
    }

    static async statistics( $ ) {

        $.set('Cache-Control', 'no-store, no-cache, must-revalidate');
        var statistics = await ServerStatistics.getInstance();
        
        statistics = statistics.toObject();
        statistics.users = await User.countDocuments();
        statistics.reviews = await User.countDocuments( { games: { $gt: 1 } } );
        statistics.liveGames = statistics.liveGames.reverse();
        
        var response = { statistics };

        if( $.isAuthenticated)
            response.balance = $.isAuthenticated.balance;

        return $.body = response;
    }
}

module.exports = RouletteController;