
const axios = require( 'axios' );
const querystring = require( 'querystring' );

const User = require( '../../models/user' );
const ServerStatistics = require( '../../models/server-statistics' );

const { VK } = require( 'vk-io' );

class AuthController {

    static async login( $ ) {

        console.log( 'LOGIN' );
        console.log( $.refreshTokens );
        $.authRedirect( $ );
    }

    static async vk( $ ) {

        var code = $.query.code;
        var error = $.query.error;

        if( code ) {

            // Получаем токен с помощью полученного кода.
            var url = "https://oauth.vk.com/access_token?" + querystring.stringify( { ...CONFIG.authorization.vk, code  } );
            var data = ( await axios.get( url ).catch( () => {} ) || { data: {} } ).data;
            
            if( data.access_token ) {

                // подключаемся к VK API.
                var vk = new VK( { token: data.access_token } );
    
                // загружаем данные о пользователе
                var user = (await vk.api.users.get({
                    user_id: data.user_id,
                    fields: "photo_200",
                }))[0];
    
                // выбираем нужные данные пользователя и закидываем их в БД
                var update = {
                    id: data.user_id,
                    token: data.access_token,
    
                    first_name: user.first_name,
                    last_name: user.last_name,
                    avatar: user.photo_200,
                }
                var user = await User.findOne( { id: update.id } );

                if( !user ) {

                    var stats = await ServerStatistics.getInstance();
                    update.balance = stats.regBonus;
                }

                user = await User.findOneAndUpdate( { id: update.id }, update, { upsert: true, new: true } );

                await $.refreshTokens( user );
                return $.redirect( '/' );
            }
            
            return $.redirect( '/' );
    
            // Если это не пользователь и не сообщества, значит ошибка.
            // ( в крайнем случае какая-то неведомая хуйня ).
        }
        
        if( error ) {
            // TODO: обработка ошибок.
        }
        
        $.redirect( '/' );
    }

    static logout( $ ) {

        $.logout();
        $.redirect( '/' );
    }
}

module.exports = AuthController;