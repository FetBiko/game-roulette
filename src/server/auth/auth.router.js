const router        = require( 'koa-router' )();
const controller    = require( './auth.controller' );

// router
//     .param( 'community',    controller.paramCommunity )

router
    .get( '/login',         controller.login )
    .get( '/logout',        controller.logout )
    .get( '/login/vk',      controller.vk );

module.exports = router;