const { combine } = require( '../lib/helpers/router' );

const requireLogin = require( '../middleware' ).authorizaton.requireLogin;

var isAdministrator = () => {

    return async ( $, next ) => {

        if( $.isAuthenticated && $.isAuthenticated.role.toLowerCase() == 'administrator' )
            return await next();

        $.redirect( '/auth/login' );
    }
}
const router = combine( [
    {
        prefix: '/auth',
        router: require( './auth/auth.router' ),
        middlewares: [  ]
    },
    {
        prefix: '/management',
        router: require( './management/management.router' ),
        // middlewares: [ requireLogin() ]
        middlewares: [ isAdministrator() ]
    },
    // {
    //     prefix: '/uploads',
    //     router: require( './uploads/uploads.router' ),
    //     middlewares: [  ]
    // },
    {
        prefix: '/roulette',
        router: require( './roulette/roulette.router' ),
        middlewares: [  ]
    },
    {
        prefix: '/payments',
        router: require( './payments/payments.router' ),
        middlewares: [  ]
    }
] );

module.exports = router;
