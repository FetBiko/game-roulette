const router        = require( 'koa-router' )();
const multer =  require( 'koa-multer');

const upload = multer(  {
    dest: __rootname + '/tmp/uploads/',
    limits: {
        files: 1
    }    
} );

router
    .post( '/image',  upload.single( 'file' ), $ => {

        console.log( $.request );
    } );

module.exports = router;