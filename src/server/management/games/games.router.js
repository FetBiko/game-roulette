
const router        = require( 'koa-router' )();
const controller    = require( './games.controller' );

// router
//     .param( 'community',    controller.paramCommunity )

router.use(  );

router
    .get( '/',              controller.getGames   )
    .post( '/',             controller.createGame )
    .post( '/:gameid',      controller.parseParams, controller.updateGame )
    .delete( '/:gameid',    controller.parseParams, controller.deleteGame )
    .get( '/:gameid',       controller.parseParams, controller.getGame    )

    
module.exports = router;