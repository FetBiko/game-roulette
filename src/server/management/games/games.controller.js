const Game = require( '../../../models/game' );
const json5 = require( 'json5' );

class GamesController {

    static async parseParams( $, next ) {

        if( $.params.gameid ) {

            $.game = await Game.findOne( { id: $.params.gameid } );

            if( !$.game ) {

                return $.body = 'Page not found.';
            }
        }

        await next();
    } 
 
    static async createGame( $ ) {

        var body = $.request.body;
        var errors = false;

        // var keys = require( '../../../lib/helpers/steam/random-keys' )( Math.floor( Math.random() * 900 + 100 ), ',' ).split( ',' );
        var game = await Game.create( {
            title: body.title,
            image: body.image,
            liveImage: body.liveImage,
            steamPrice: isNaN( body.steamPrice ) ? null : body.steamPrice,
            sellPrice: isNaN( body.sellPrice ) ? null : body.sellPrice,
            steamPage: body.steamPage,
            keys: body.keys || [],
            isTwisted: body.isTwisted || false
        } ).catch( error => {

            errors = error.errors;
        });

        if( errors )
            return $.body = { errors }
        
        $.body = { id: game.id };
    }

    static async updateGame( $ ) {

        var body = $.request.body;

        var update = {
            title: body.title,
            image: body.image,
            liveImage: body.liveImage,
            steamPrice: isNaN( body.steamPrice ) ? null : body.steamPrice,
            sellPrice: isNaN( body.sellPrice ) ? null : body.sellPrice,
            steamPage: body.steamPage || null,
            isTwisted: body.isTwisted || false
        }, errors = false;

        console.log( update.liveImage );
        
        if( isNaN( update.steamPrice ) )
            update.steamPrice = null;

        try {
            await $.game.updateOne( update, { runValidators: true } )
            $.body = { gameid: $.game.id };
        } catch( error ) {
            $.body = { error }
        }
    }
    
    static async deleteGame( $ ) {

        console.log( 'delteGame',  );

        await $.game.remove()
            .then( result => {

                $.body = { success: 1 }
            })
            .catch( error =>{ 
                $.body = { error }
            });
    }

    static async getGame( $ ) {

        $.render( 'management/games/page', {
            authorized: $.isAuthenticated,
            game: $.game
        });
        // $.body = 'Game with id ' + $.params.gameid;
    }

    static async getGames( $ ) {

        var games = await Game.find();
        
        var counters = {
            documents: await Game.countDocuments()
        }

        $.render( 'management/games', {
            authorized: $.isAuthenticated,
            counters,
            games
        } );
    }

}

module.exports = GamesController;