const { combine } = require( '../../lib/helpers/router' );

const router        = require( 'koa-router' )();
const controller    = require( './management.controller' );
const ServerStatistics    = require( '../../models/server-statistics' );

// router
//     .param( 'community',    controller.paramCommunity )

router
    .get( '/', async $ => {

        var serverStatistics = await ServerStatistics.getInstance();

        console.log( serverStatistics.balanceMultiplier );

        $.render( 'management/management', {
            authorized: $.isAuthenticated,
            multiplier: serverStatistics.balanceMultiplier,
            register_bonus: serverStatistics.regBonus
        } );
    } )
    .post( '/multiplier', async $ => {

        var params = $.request.body;
        var multiplier = parseFloat( params.multiplier );
        console.log( multiplier );

        if( !multiplier || multiplier < 1  || isNaN( multiplier ) )
            return $.body = { errors: [ 'invalid_multiplier' ] };

        var serverStatistics = await ServerStatistics.getInstance();
            serverStatistics.balanceMultiplier = multiplier;

            await serverStatistics.save();

        return $.body = { success: serverStatistics.balanceMultiplier }
    })
    .post( '/regbonus', async $ => {

        var params = $.request.body;
        var regbonus = parseFloat( params.regbonus );

        if( !regbonus || regbonus < 0  || isNaN( regbonus ) )
            return $.body = { errors: [ 'invalid_regbonus' ] };

        var serverStatistics = await ServerStatistics.getInstance();
            serverStatistics.regBonus = regbonus;

        await serverStatistics.save();

        return $.body = { success: serverStatistics.regBonus }

    })


module.exports = combine( [
    {
        prefix: '/games',
        router: require( './games/games.router' ),
        middlewares: []
    },
    {
        prefix: '/users',
        router: require( './users/users.router' ),
        middlewares: []
    },
    {
        prefix: '/keys',
        router: require( './keys/keys.router' ),
        middlewares: []
    }
], router );