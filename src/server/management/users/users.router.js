
const router        = require( 'koa-router' )();
const controller    = require( './users.controller' );

// router
//     .param( 'community',    controller.paramCommunity )

router.use( controller.parseParams );

router
    .get( '/',              controller.getUsers   )
    .post( '/:userid',      controller.parseParams, controller.updateUser )
    .delete( '/:userid',    controller.parseParams, controller.deleteUser )
    .get( '/:userid',       controller.parseParams, controller.getUser    )

    
module.exports = router;