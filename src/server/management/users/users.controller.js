const User = require( '../../../models/user' );
const json5 = require( 'json5' );

const { stringifyQuery, parseQuery } = require( '../../../lib/helpers/formatters'  );

class UsersController {

    static async parseParams( $, next ) {

        if( $.params.userid ) {

            $.user = await User.findOne( { id: $.params.userid } );

            if( !$.user ) {

                return $.body = 'Page not found.';
            }
        }

        await next();
    }

 
    static async updateUser( $ ) {
 
        
        console.log( 'update', $.request.body );

        var update =  {
            balance: $.request.body.balance,
            role: $.request.body.role,
            isTwisted: $.request.body.isTwisted
        };

        try {
            await $.user.updateOne( update, { runValidators: true } );

            $.body = { userid: $.user.id };
        } catch( error ) {

            $.body = { error }
        }
    }
    
    static async deleteUser( $ ) {
        $.body = { error: new Error( 'unavaliable_method' )}
    }

    static async getUser( $ ) {


        return $.render( 'management/users/profile', {
            authorized: $.isAuthenticated,
            profile: $.user
        });
    }

    static async getUsers( $ ) {

        var count = parseInt( $.query.count ) || 10, page = parseInt( $.query.page ) || 0, skip = ( page * count ) || 0, search = $.query.search || {};
        var query = $.query || {};

        try {
            search = json5.parse( search );
        } catch( e ) {
            search = {};
        }

        var users = await User.find( search ).skip( skip ).limit( count );

        var counters = {
            documents: await User.countDocuments( search )
        }
        
        var pagination = {}

        console.log( Object.assign( query, { page: 0 } ) );

            pagination.first = { href: '/management/users?' + stringifyQuery( Object.assign( query, { page: 0 } ) ), page: 'В начало' };
            pagination.last = { href: '/management/users?' + stringifyQuery( Object.assign( query, { page: counters.documents / count - 1 } ) ) , page: 'В конец' };
            pagination.current = { href: '/management/users?' + stringifyQuery( query ), page };
            pagination.pages = [];

        for( var i = -2; i < 2; i++ ) {

            if( page + i >= 0 && ( page + i ) <= counters.documents / count - 1  ) {

                var qtemp = Object.assign( {}, query, { page: page + i } );

                pagination.pages.push( { href: '/management/users?' + stringifyQuery( qtemp ), page: page + i } );

                if( page + i === 0 )
                    pagination.first = false;
                
                if( page + i === counters.documents / count - 1 )
                    pagination.last = false;
            }
        }
    
        $.render( 'management/users', {
            authorized: $.isAuthenticated,
            users,
            counters,
            pagination,
            search: search
        });
    }

}

module.exports = UsersController;