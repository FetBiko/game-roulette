
const router        = require( 'koa-router' )();
const controller    = require( './keys.controller' );

router
    .get( '/',              controller.addKeysPage   )
    .get( '/clear',              controller.clearKeys   )
    .post( '/',             controller.addKeys       )
    
module.exports = router;