const Game = require( '../../../models/game' );
const arrayChunk = require( 'array.chunk' );

class KeysController {

    static async clearKeys( $ ) {

        var games = await Game.find();
        var promises = [], errors = [];

        games.forEach( ( game ) => {
        
            promises.push( game.update( { keys: [] })
                .catch( error => {
                    errors.push( error );
                }) );

        });

        await Promise.all( promises )

        $.body = { errors, success: 1 }
    }

    static async addKeysPage( $ ) {

        var games = await Game.find();

        $.render( 'management/keys',  {
            authorized: $.isAuthenticated,
            games,
        } )
    }

    static async addKeys( $ ) {
        
        var errors = [];

        var body = $.request.body;

        console.log( 'body', body );

        if( !body.games )
            errors.push( { message: 'Нужно выбрать хотя бы одну игру!' } );

        if( !body.keys )
            errors.push( { message: 'Нужно написать список ключей!' } );

        if( errors.length > 0 )
            return $.body = { errors }

        var keys = body.keys.split( '\n' ).filter( key => !!key );

        if( keys.length == 0 ) {
            errors.push( { message: 'Нужно написать список ключей' } );
            return $.body = { errors }
        }

        var games = body.games;

        var games = await Game.find( { id: { $in: games } } )
            .catch( errors => errors.push( { message: 'Не удалось загрузить игры. Попробуйте ещё раз!' }))

        if( !games || games.length < 1 ) 
            errors.push( { message: 'Нужно выбрать хотя бы одну игру!' } );

        if( errors.length > 0 )
            return $.body = { errors }

        var keys = arrayChunk( keys, Math.floor( keys.length / games.length ) );
        var promises = [];

        games.forEach( ( game, indx ) => {
            
            var currentGameKeys = keys[indx];

            if( !currentGameKeys )
                return errors.push( { message: `Для игры №${game.id} (${game.title}) не достались ключи!` } );

            game.keys.push( ...currentGameKeys );

            promises.push( game.save().catch( error => {
                errors.push( { message: `Не удалось сохранить ключи для игры №${game.id} (${game.title}) не достались ключи!` } );
            }));
        });

        await Promise.all( promises )

        $.body = { errors: errors.length > 0 ? errors : false, success: 1 }
    }
}

module.exports = KeysController;