const mongoose = require( 'mongoose' );
const autoIncrement = require( 'mongoose-auto-increment' );

const Schema = mongoose.Schema;

const winSchema = new Schema({
    
    id: {
        type: Number,
        unique: true,
        index: true
    },

    user: {
        type: Number,
        required: true
    },

    game: {
        type: Number,
        required: true
    },

    key: {
        type: String
    },

    isCompleted:  {
        type: Boolean,
        default: false
    }

}, { timestamps: true } );

winSchema.plugin( autoIncrement.plugin, { model: 'Win', field: 'id' } )


module.exports = mongoose.model( 'Win', winSchema );