const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

const autoIncrement = require( 'mongoose-auto-increment' );

const gameSchema = new Schema({
    
    id: {
        type: Number,
        required: true,
        index: true
    },
    title: {
        type: String,
        required: true
    },

    image: {
        type: String,
        required: true
    },
    liveImage: {
        type: String
    },

    steamPrice: {
        type: Number
    },

    sellPrice: {
        type: Number
    },

    steamPage: {
        type: String
    },

    keys: {
        type: [String],
        default: []
    },

    isTwisted: {
        type: Boolean,
        default: false
    }
}, { autoIndex: true } );

gameSchema.plugin( autoIncrement.plugin, { model: 'Game', field: 'id' } )

module.exports = mongoose.model( 'Game', gameSchema );