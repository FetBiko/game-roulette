const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

const serverStatisticsSchema = new Schema({
    
    liveGames: {
        type: Array,
        default: []
    },
    liveSum: {
        type: Number,
        default: 0
    },

    topGames: {
        type: Number,
        default: 10
    },

    balanceMultiplier: {
        type: Number,
        default: 1
    },

    regBonus: {

        type: Number,
        default: 19
    }
});


serverStatisticsSchema.statics = {

    getInstance: function () {

        return new Promise( ( resolve, reject ) => {

            this.findOne()
                .sort( { updated: -1 } )
                .limit( 1 )
                .exec( ( error, model ) => {

                    if (error) {
                        
                        return reject( error )
                    } 

                    if (model == null) {
                        return resolve( new ServerStatistics() );
                    }

                    resolve( model );
                });
        })
        
    },
};

serverStatisticsSchema.method( 'pushGame', function( image, name ) {
    
    if( !this.liveGames ) this.liveGames = [];

    this.liveGames.push( { image, name } );

    while( this.liveGames.length > 9 )
        this.liveGames.shift();

});

const ServerStatistics = mongoose.model( 'ServerStatistics', serverStatisticsSchema );


module.exports = ServerStatistics;