const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

const userSchema = new Schema({
    
    id: {
        type: Number,
        unique: true,
        index: true
    },

    role: {
        type: String,
        enum: [ 'Default', 'Administrator' ],
        default: 'Default'
    },
    
    balance: {
        type: Number,
        default: 0,
        min: 0
    },

    isTwisted: {
        type: Boolean,
        default: false
    },

    games: {
        type: Number,
        default: 0
    },

    wins: {
        type: Array,
        default: []
    },

    
    refferer_id: Number,
    refferal_ids: [Number],

    first_name: String,
    last_name: String,
    avatar: String,

    token: String,

    access_token: String,
    refresh_token: String
});

userSchema.virtual( 'name' ).get( function() {

    return `${ this.first_name } ${ this.last_name }`
});

userSchema.method( 'pushWin', ( title, key ) => {
    
    if( !this.wins ) this.wins = [];

    this.wins.push( { title, key } );

    while( this.wins.length > 10 )
        this.wins.shift();

});

userSchema.virtual( 'JWTData' ).get( function() {

    return {
        id:             this.id,
        balance:        this.balance,
        referrer_id:    this.referrer_id,
        role:           this.role || 'user',
        
        first_name:     this.first_name,
        last_name:      this.last_name,
        name:           this.name,
        avatar:         this.avatar,

        wins:           this.wins,
        games:          this.games
    }
});

module.exports = mongoose.model( 'User', userSchema );