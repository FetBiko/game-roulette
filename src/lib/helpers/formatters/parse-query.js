module.exports = ( queryString = {} ) => {
    var query = {};
    var pairs = ( queryString[0] === '?' ? queryString.substr(1) : queryString ).split('&');

	if( !pairs  || pairs.length === 0 || pairs[0] === '' )
		return false;

	console.log( pairs );
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }

    return query;
}