module.exports = {

    formatNumber: require( './format-number' ),
    parseQuery: require( './parse-query' ),
    stringifyQuery: require( './stringify-query' )
}