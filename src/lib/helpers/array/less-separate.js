
var lessSeparate = ( arr, l = 1 ) => {

    var less = [];
    var same = [];
    
    arr.forEach( ( v, i ) => {
        
        if( !Array.isArray( v ) )
            v = [v];

        if( v.length <= l ) {

            less.push( ...v );
        } else {

            same.push( v );
        }
    });

    return { same, less }
}

// var arry = [ [ 1 ],
//   [ 2, 2 ],
//   [ 3, 3, 3 ],
//   [ 4, 4, 4, 4 ],
//   [ 5, 5 ],
//   [ 6 ],
//   [ 7, 7, 7 ],
//   [ 8, 8, 8 ],
//   [ 9, 9, 9 ] ];

// console.log( lessSeparate( arry, 1 ) );

module.exports = lessSeparate;