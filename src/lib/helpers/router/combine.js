
module.exports = ( routes, root = require( 'koa-router' )() ) => {
    
    routes.forEach( r => {
        
        if( r.routes && r.allowedMethods )
            root.use( r.routes(), r.allowedMethods() )
        
        if( r.router.routes && r.router.allowedMethods ) {

            if( r.middlewares && typeof r.middlewares == 'function' )
                r.middlewares = [ r.middlewares ];

            r.prefix 
                ? r.middlewares 
                    ? root.use( r.prefix, ...r.middlewares, r.router.routes(), r.router.allowedMethods() )
                    : root.use( r.prefix, r.router.routes(), r.router.allowedMethods() )
                : r.middlewares 
                    ? root.use( ...r.middlewares, r.router.routes(), r.router.allowedMethods() )
                    : root.use( r.router.routes(), r.router.allowedMethods() )
        }
    });

    return root;
}