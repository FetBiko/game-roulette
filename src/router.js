const root = require( 'koa-router' )();

const { combine, map } = require( './lib/helpers/router' );

const server = require( './server/router' );

const ServerStatistics = require( './models/server-statistics' );
const User = require( './models/user' );
const Game = require( './models/game' );
const Win = require( './models/win' );

var auth = require( './middleware/authorization' );

root.get( '/', auth.authorization(), async ( $ ) => {

    var user = $.isAuthenticated;


    var statistics = await ServerStatistics.getInstance();

    statistics = statistics.toObject();
    statistics.users = await User.countDocuments();
    statistics.reviews = await User.countDocuments( { games: { $gt: 1 } } );
    statistics.liveGames = statistics.liveGames.reverse();
    
    var topGames = await Game.find( { isTwisted: true } );

    var wins = false, payments = false;

    if( user ) {

        payments  = {
            shop: CONFIG.payments.shopId,
            payment: user.id + '' + Date.now(),
            description: CONFIG.payments.description || 'GOODGAMES',
            currency:  CONFIG.payments.currency
        };

        // payments.sign = ;
        
        var _wins = await Win.find( { user: user.id } ).limit( 5 ).sort( { id: -1 });
        wins = [];

        await Promise.all( _wins.map( async win => {

            var game = await Game.findOne( { id: win.game } );

            wins.push( {
                ...win,
                game
            });
        }) );

        console.log( wins );

        if( wins == [] ) wins = false;
    }

    var gamelist = await Game.find().sort( { steamPrice: -1 });

    $.render( 'index', {
        user: $.isAuthenticated,
        statistics,
        topGames,
        wins,
        gamelist,
        payments,
        isDemo: !!user
    });
});

root.get( '/wins', auth.authorization(), auth.requireLogin(), async ( $ ) => {

    var params = $.query;

    var count = 5, offset = parseInt( params.offset ) || 0;

    var wins = false;

    if( $.isAuthenticated ) {
        
        var _wins = await Win.find( { user: $.isAuthenticated.id } ).skip( offset ).sort( { id: -1 } ).limit( count );
        var count = await Win.countDocuments( { user: $.isAuthenticated.id } );
        wins = [];

        await Promise.all( _wins.map( async win => {

            var game = await Game.findOne( { id: win.game } );

            wins.push( {
                win: win.id,
                game: {
                    title: game.title,
                    image: game.liveImage || game.image
                }
            });
        }) );
    } else return  $.body = { error: 'internal_error' }

    if( wins ) wins.sort( ( a, b ) => a.win < b.win )
    return $.body = { wins, count };
})


root.get( '/wins/:winid', auth.authorization(), auth.requireLogin(), async ( $ ) => {

    if( !$.isAuthenticated ) return  $.body = { error: 'internal_error' }

    var win = await Win.findOne( { id: $.params.winid } );

    if( !win || win.user != $.isAuthenticated.id )  return $.body = { errors: [ 'access_denied' ] }

    var game = await Game.findOne( { id: win.game } );

    win = {
        id: win.id,
        isCompleted: win.isCompleted,
        key: win.key || false,
        user: win.user
    }

    game = {
        id: game.id,
        title: game.title,
        image: game.image,
        steamPrice: game.steamPrice,
        sellPrice: game.sellPrice || CONFIG.roulette.sellPrice
    }

    return $.body = { win: win, game: game };
})

const router = combine( [
    {
        router: server,
        middlewares: [ auth.authorization()  ]
    }
], root );

console.log( 'MAP:\n' + map( router ) );
module.exports = router;