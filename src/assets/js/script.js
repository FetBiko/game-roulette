var $ = require('jquery'),
    magnificPopup = require('magnific-popup');

$('.rullet__controls-buy').on('click', roll );


if( $('.cabinet-link').length ) {
    $('.cabinet-link').magnificPopup({
        items: {
            src: '#cabinet-popup',
            type: 'inline'
        },

        callbacks: {
            open: function() {
                console.log('this');
                $('.popup__close').on('click', function(){
                    $.magnificPopup.close();
                });
            },
        }
    });
}

if( $('#terms-link').length ) {
    $('#terms-link').magnificPopup({
        items: {
            src: '#terms-popup',
            type: 'inline'
        },

        callbacks: {
            open: function() {
                console.log('this');
                $('.popup__close').on('click', function(){
                    $.magnificPopup.close();
                });
            },
        }
    });
}

if( $('#win-link').length ) {
    $('#win-link').magnificPopup({
        items: {
            src: '#win-popup',
            type: 'inline'
        },

        callbacks: {
            open: function() {
                console.log('this');
                $('.popup__close').on('click', function(){
                    $.magnificPopup.close();
                });
            },
        }
    });
}

if( $('#chance-link').length ) {
    $('#chance-link').magnificPopup({
        items: {
            src: '#chance-popup',
            type: 'inline'
        },

        callbacks: {
            open: function() {
                console.log('this');
                $('.popup__close').on('click', function(){
                    $.magnificPopup.close();
                });
            },
        }
    });
}


function rollAnimation( response ) {

    var game = response.game;

    $('#rullet .rullet__items-container').css({
        transition: '0s',
        transform: 'rotate(0)'
    });

    setTimeout( function() {

        $('#rullet .rullet__items-container').css({
            transition: '4s cubic-bezier( 0, .5, .25, 1 )',
            transform: 'rotate(1080deg)'
        });

        $( '.rullet__controls-buy' ).attr( 'disabled', 'disabled' );
        
        setTimeout( function() {

            shuffleGames();
            $( '.rullet__items-container .rullet__item:first-child img' ).attr('src', game.image );
        }, 550 );

        setTimeout( function() {

            $( '.rullet__controls-buy' ).removeAttr( 'disabled' );

            console.log( 'win!' );
            setTimeout( rollReset, 250 );

            if( response.isDemo )
                return winnerDemoPopup( game, response.warn );

            
            winnerPopup( response );
        }, 4050 );

    }, 50);

}

function shuffleArr(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function rollReset() {

    console.log( 'rollReset' );

    shuffleGames();
    $('#rullet .rullet__items-container').css({
        transition: '1s',
        transform: 'rotate(0)'
    });
}

var rouletteImages = [];

for( var i = 28; i > 0; i-- ) {
    rouletteImages.push( '/assets/img/g/' + i + '.png' );
}


function shuffleGames() {

    rouletteImages = shuffleArr( rouletteImages );

    var games = Array.from( $( '.rullet__items-container .rullet__item img' ) );

    games.forEach( function( game, index ) {
        
        game.setAttribute( 'src', rouletteImages[ index ] );
    });
}
shuffleGames(); 

function winnerPopup( response ) {

    console.log( response );

    var game = response.game;
    var win = response.win;

    $( '.popup.win .popup__title' ).text( 'Вы выиграли ' + game.title );
    $( '.win__game img' ).attr( 'src', game.image );

    $( '.win__code' ).css( { display: 'none' }).text( 'XXXXX-XXXXX-XXXXX' );

    $( '.win__code' ).css( { fontSize: 24 });
    $( '.win__info' ).css( {
        display: game.price || game.steamPrice ? 'flex' : 'none'
    });

    $( '#winner_sell' ).css( {
        display: 'flex'
    }).text( 'Продать за ' + game.sellPrice + '₽' );

    $( '#winner_warn' ).css( {
        display: 'none'
    });

    $( '#winner_key' ).css( {
        display: 'flex'
    });
    

    $( '#winner_sell' ).click( function() {

        sellGame( win );
        // Продать игру, закрыть окно, обновить баланс.
    });

    $( '#winner_key' ).click( function() {

        keyGame( win );
        // Получить ключ, показать ключ.
    });

    $( '.win__info .color_green' ).text( ' ' + game.price );

    $.magnificPopup.open({
        items: {
            src: '#win-popup',
            type: 'inline'
        },

        callbacks: {

            open: function() {
                console.log('this');

                $('.close_btn').on('click', function(){
                    $.magnificPopup.close();
                });
                
            }
        }
    });
}

function winnerDemoPopup( game,  warn ) {

    $( '.popup.win .popup__title' ).text( 'В демо-режиме вы выиграли ' + game.title );
    
    $( '.win__game img' ).attr( 'src', game.image );
    $( '.win__code' ).text( 'Чтобы попробовать выиграть ' + game.title + ' по-настоящему:' );

    $( '#winner_sell' ).css( {
        display: 'none'
    });

    $( '#winner_warn' ).css( {
        display: 'flex'
    });

    $( '#winner_key' ).css( {
        display: 'none'
    });

    $( '.win__info' ).css( {
        display: game.price ? 'flex' : 'none'
    });

    $( '.win__code' ).css( { fontSize: 16 });

    $( '.win__info .color_green' ).text( ' ' + game.price );

    if( warn == 'need_login' ) {
        $( '#winner_warn' ).text( 'Войдите' );

        $( '#winner_warn' ).attr( 'href', '/auth/login' )
    }
    if( warn == 'balance_low' ) {
        $( '#winner_warn' ).text( 'Пополните счёт' );

        $( '#winner_warn' ).click( function() {

            setTimeout( function() {
                $.magnificPopup.open({
                    items: {
                        src: '#cabinet-popup',
                        type: 'inline'
                    },
            
                    callbacks: {
            
                        open: function() {
                            console.log('this');
            
                            $('.popup__close').on('click', function(){
                                $.magnificPopup.close();
                            });
                        },
                    }
                });
            }, 100 );
            
        })
        
    }

    $.magnificPopup.open({
        items: {
            src: '#win-popup',
            type: 'inline'
        },

        callbacks: {

            open: function() {
                console.log('this');

                $('.popup__close').on('click', function(){
                    $.magnificPopup.close();
                });
            },
        }
    });
}

function openWin( id ) {

    $.ajax({
        type: 'GET',
        url: '/wins/' + id,  
        success: function( response ) {

            console.log( response );
            if( response.error || response.errors ) 
                return;
            
            var game = response.game;
            var win = response.win;

            console.log( 'steamPrice:', game.steamPrice );

            $( '.popup.win .popup__title' ).text( 'Вы выиграли ' + game.title );
            $( '.win__info .color_green' ).text( ' ' + game.steamPrice );
            $( '.win__game img' ).attr( 'src', game.image );

            if( win.isCompleted ) {

                $( '.win__code' ).css( { display: 'block' }).text( win.key ? win.key : 'Игра продана!' );

                $( '#winner_sell' ).css( { display: 'none' });
                $( '#winner_key' ).css( { display: 'none' });
    
                $( '#winner_warn' ).css( { display: 'flex' }).text( 'Закрыть' );
            } else  {

                $( '.win__code' ).css( { display: 'none'  } ).text( win.key ? win.key : 'Игра продана!' );

                $( '#winner_warn' ).css( { display: 'none' });

                
                $( '#winner_sell' ).css( { display: 'flex' }).text( 'Продать за ' + game.sellPrice + '₽' ).click( function() {
                    sellGame( win.id );
                });
            
                $( '#winner_key' ).css( { display: 'flex' }).click( function() {
                    keyGame( win.id );
                });
            
            }

            $.magnificPopup.open({
                items: {
                    src: '#win-popup',
                    type: 'inline'
                },

                callbacks: {

                    open: function() {
                        console.log('this');

                        $('.close_btn').on('click', function(){
                            $.magnificPopup.close();
                        });
                    },
                }
            });


        }
    });



}

function updateBalance( amount ) {

    if( !$( '.header__profile-amount' ) || !amount ) return;

    $( '.header__profile-amount' ).text( Math.floor( amount ).toLocaleString( 'ru' ).split( ',' ).join( ' ' ) + '₽'  );
    $( '.rullet__controls-helper').css( { display: amount >= 59 ? 'none' : 'block' } );

    if( $( '.header__profile-login' ).length ) {
        $( '.rullet__controls-helper').css( { display: 'none' } );
    }
}

if( $( '.header__profile-login' ).length ) {
    $( '.rullet__controls-helper').css( { display: 'none' } );
}

function roll() {

    $.ajax({
        type: 'POST',
        url: '/roulette/roll',
        success: function( response ) {

            if( response.errors ) {

                return console.error( response.errors );
            }

            rollAnimation( response );

            if( response.balance )
                updateBalance( response.balance );

            refreshWins();
        }
    });
}


function sellGame( win ) {

    $.ajax({
        type: 'POST',
        url: '/roulette/sell',
        data: { winid: win },
        success: function( response ) {

            updateBalance( response.balance );
            openWin( win );

            console.log( 'sellGame', response );
        }
    });
}


function keyGame( win ) {

    $.ajax({
        type: 'POST',
        url: '/roulette/key',
        data: { winid: win },  
        success: function( response ) {

            openWin( win );
            $( '.win__code' ).css( { display: 'block' }).text( response.key );
            $( '#winner_sell' ).css( { display: 'none' });
            $( '#winner_key' ).css( { display: 'none' });

            $( '#winner_warn' ).css( { display: 'flex' }).text( 'Закрыть' );
        }
    });
}

function update() {

    $.ajax({
        type: 'POST',
        url: '/roulette/statistics', 
        success: function( response ) {

            console.log( response );

            if( response.balance ) {
                updateBalance( response.balance );

                $( '.rullet__controls-buy' ).text( response.balance >= 59 ? 'Крутить за 59₽' : 'Крутить бесплатно!' );
            }

            $( '.live__stats' ).html( 'Сегодня выпало игр на: ' + response.statistics.liveSum.toLocaleString( 'ru' ).split( ',' ).join( ' ' ) + ' ₽ <div class="live__stats-online"></div>' );

            var liveGames = '';

            response.statistics.liveGames.forEach( function( item ) {

                    liveGames += '<div class="live__games-item"><img src="' + item.image + '"><div class="live__games-item-hint">' + item.name + '</div></div>';
            });

            $( '.live__games' ).html( liveGames );

            $( '#users_stat' ).text( response.statistics.users.toLocaleString( 'ru' ).split( ',' ).join( ' ' ) );
            $( '#reviews_stat' ).text( response.statistics.reviews.toLocaleString( 'ru' ).split( ',' ).join( ' ' ) );
            $( '#games_stat' ).text( response.statistics.topGames.toLocaleString( 'ru' ).split( ',' ).join( ' ' ) );

        }
    });
}

setInterval( update, 10000 );

var winsOffset = 0, winsCount = 5;

function scrollWins( type ) {

    var checker = winsOffset;
    console.log( 'refreshWins', winsOffset, 'winsCount', winsCount );

    if( type == 'next' )
        winsOffset += 5;

    if( type == 'prev' )
        winsOffset -= 5;

    if( winsOffset < 0 )    winsOffset = 0;
    if( winsOffset + 5 > winsCount )    winsOffset = winsCount - 5;

    console.log( 'refreshWins', winsOffset );

    if( winsOffset == checker ) return;
    
    $( '.cabinet__wins-container' ).attr( 'loading', 'true' );

    $.ajax({
        type: 'GET',
        url: '/wins?offset=' + winsOffset, 
        success: function( response ) {

            console.log(  response );

            if( !response.error ) {

                if( response.wins  && response.wins.length > 0 ) {
                    winsCount = response.count;

                    var newHtml = '<div class="cabinet__wins-item-arrow" id="left_arrow"></div>';

                    response.wins.forEach( function( win ) {

                        newHtml += '<div class="cabinet__wins-item" value="' + win.win + '"><img src="' + win.game.image + '"><div class="cabinet__wins-item-hint">' + win.game.title + '</div></div>';
                    });

                    newHtml += '<div class="cabinet__wins-item-arrow" id="right_arrow"></div>';

                    $( '.cabinet__wins-container' ).html( newHtml );

                    $( '.cabinet__wins-container #right_arrow' ).click(  function() {

                        scrollWins( 'next' );
                    });

                    $( '.cabinet__wins-container #left_arrow' ).click( function() {

                        scrollWins( 'prev' );
                    });
                } else {

                    $( '.cabinet__wins-container' ).text( 'Выигрышей пока нет!' );
                }

            }
            $( '.cabinet__wins-container' ).attr( 'loading', 'false' );

            $( '.cabinet__wins-item' ).click( function() {

                console.log( 'win',this);
                openWin( this.getAttribute( 'value' ) ); 
            });

        }
    });
}

function refreshWins() {

    winsOffset = 5;
    scrollWins( 'prev' );
}

refreshWins();

function payment( amount ) {

    $.ajax({
        type: 'POST',
        url: '/payments/payment', 
        data: { amount: amount },
        success: function( response ) {

            console.log( response );
        }
    });
}


$( 'button[name="payment_submit"]').click( function() {

    var amount = $( 'input[name="amount"]').val();
    payment( amount );
});

shuffleGames();