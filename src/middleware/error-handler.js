module.exports = () => {

    return async ( $, next ) => {
        try {

            await next();
        } catch( error ) {

            console.log( error );

            if( $.isAuthenticated && $.isAuthenticated.role == 'Administrator' )
                return $.body = { error: error }

            $.status = error.status || 500;
            $.body = { errors: [ 'internal' ], code: 0 }
        }
    }

}