module.exports = () => {

    return async ( $, next ) => {
        
        try {

            var counter = await require( '../lib/counters/counters' )();

            $.counters = counter.values() || {
                winners: 5000,
                reviews: 400,
                topGames: 100,
            
                gameAmount: 1000,
                
                liveGames: [],
                todayUpdates: 0
            }

        } catch( e ) {

            console.error( e );
        }
        await next();
    }
}