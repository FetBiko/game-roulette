const jwt = require( 'jsonwebtoken' );
const uuidv4 = require( 'uuid/v4' );
const querystring = require( 'querystring' );

const User = require( '../../models/user' );

var _authRedirect = $ => {

    return ()  => {

        $.redirect( 'https://oauth.vk.com/authorize?' + querystring.stringify( { ...CONFIG.authorization.oauth.user } ) );
    }
}

var _accessToken = token => {
    try {
        
        return jwt.verify( token, CONFIG.authorization.private_key );
    } catch( err ) {
        console.error( 'at:',  err );

        return false;
    }
}
var _refreshToken = async token => {
    try {
        
        jwt.verify( token, CONFIG.authorization.private_key );
        var user = await User.findOne( { refresh_token: token } );

        console.log( 'rt user:', user, token )
        return user ? user : false;
    } catch( err ) {

        console.error( 'rt:',  err );
        return false;
    }
}
var _refreshTokens = $ => {

    return async user => {
        var accessToken = jwt.sign( user.JWTData, CONFIG.authorization.private_key, { expiresIn: '15m' });
        var refreshToken = jwt.sign( { rand: uuidv4() },  CONFIG.authorization.private_key, { expiresIn: '30d' } );

        user.refresh_token = refreshToken;
        user.access_token = accessToken;

        $.cookies.set( 'goodgms:access', accessToken, { httpOnly: false } );        
        $.cookies.set( 'goodgms:refresh', refreshToken,  { httpOnly: false, expires: new Date( Date.now() + 1000 * 60 * 60 * 24 * 30 ) } );

        $.isAuthenticated = user;
        
        await user.save();
        return user;
    }
}

var _logout = ( $ ) => {

    return () => {
        $.cookies.set( 'goodgms:access', null );
        $.cookies.set( 'goodgms:refresh', null );
    }
}

module.exports = () => {

    return async ( $, next ) => {

        $.refreshTokens = _refreshTokens( $ );
        $.logout        = _logout( $ );
        $.authRedirect  = _authRedirect( $ );

        // Пользователь ещё не вошёл.
        if( !$.cookies.get( 'goodgms:access' ) )
            return next();

        $.isAuthenticated = _accessToken( $.cookies.get( 'goodgms:access' ) );

        // console.log( 'auth user, access token:', $.isAuthenticated );
        
        console.log( '$.isAuthenticated', $.isAuthenticated );
        // Если goodgms:access валидный, то пропускаем обновление токенов.

        if( $.isAuthenticated ) {

            $.isAuthenticated = await User.findOne( { id: $.isAuthenticated.id } );
            return next();
        }

        // Проверяем goodgms:refresh.
        user = await _refreshToken( $.cookies.get( 'goodgms:refresh' ) )
        console.log( 'user', user );

        // Если пользователь получен и goodgms:refresh валидный, то
        if( user ) {
            
            // обновляем токены.
            $.isAuthenticated = await $.refreshTokens( user );
            // console.log( '2 user:', $.isAuthenticated )

            return next();
        } else {
            
            console.log( 'Erorr auth.' );

            // иначе перекидываем на авторизацию.
            // $.logout();
            return next();
        }
    }
}