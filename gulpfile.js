var gulp = require('gulp'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),


    /* defines */
    cssSrc = './src/assets/scss/**/*.scss',
    cssSource = './src/assets/scss/style.scss',
    cssDest = './public/assets/css/'
    jsSrc = './src/assets/js/**/*.js',
    jsSource = './src/assets/js/script.js',
    jsDest = './public/assets/js/';

    /* tasks */
    gulp.task('css', () => {
        return gulp.src(cssSource)
            .pipe(sass({ outputStyle: 'compressed' }))
            .pipe(prefix({ browsers: ['last 2 versions'], cascade: false }))
            .pipe(gulp.dest(cssDest))
    });

    gulp.task('js', () => {
        return browserify(jsSource).bundle()
            .pipe(source('build.js'))
            .pipe(buffer())
            .pipe(uglify())
            .pipe(gulp.dest(jsDest))
    });

    gulp.task('watch', () => {
        gulp.watch([cssSrc], gulp.parallel(['css']));
        gulp.watch([jsSrc], gulp.parallel(['js']));
    });

    gulp.task( 'build', gulp.parallel( [ 'css', 'js' ] ) );

    gulp.task( 'default', gulp.parallel( ['watch'] ) );
